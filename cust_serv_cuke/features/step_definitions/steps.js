const {Given, When, Then, After, Before} = require('cucumber');
const puppeteer = require('puppeteer');
const mongoose = require('mongoose');

var browser;
var page;

// TODO: Replace the name of your database with the one you are using for the backend
Before(async function() {
  mongoose.connect('mongodb://localhost/name_of_database', {useNewUrlParser: true});
  browser = await puppeteer.launch({headless: false});
});

After(async function() {
  await browser.close();
  Movies.deleteMany({}, function(err) {});
  await mongoose.connection.close();
});

Given('STRS\'s ride history includes the following trips', async function (dataTable) {
  page = await browser.newPage();
  await page.goto('http://localhost:3000/lost_and_found');
});

Given('{string} lodges a customer service request for lost object', function (string) {
});

Given('the lost object is a {string}', function (string) {
});

Given('the date of the loss is {string}', async function (date_of_trip) {
  await page.$eval('#date-of-trip', (el, value) => el.value = value, date_of_trip);
});

When('the operator enters the date of loss', function () {
  // Write code here that turns the phrase above into concrete actions
  return 'pending';
});

When('the operator enters {string} as pickup address', function (string) {
  // Write code here that turns the phrase above into concrete actions
  return 'pending';
});

Then('the operator should see {int} rows', function (int) {
  // Write code here that turns the phrase above into concrete actions
  return 'pending';
});
