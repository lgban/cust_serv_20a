Feature: Lost and found feature
  As an operator of STRS's customer service
  Such that I can help a customer find his/her lost belongings
  I want to get access to the customer's taxi ride history

  Scenario: Identifying taxi ride among multiple rides on a precise date
    Given STRS's ride history includes the following trips
      | date       | pickup_time | pickup_address         | dropoff_address           | taxi_id |
      | 2020-03-15 | 16:20:10    | Via Atlixcayotl 5718   | Blvd Atlixco 3304         | 3       |
      | 2020-03-16 | 10:30:30    | Blvd Nino Poblano 2901 | Cto Juan Pablo II 3515    | 3       |
      | 2020-03-16 | 10:42:17    | Av del Sol 5           | Av Kepler 2143            | 2       |
      | 2020-03-16 | 11:32:11    | Via Atlixcayotl 5718   | Osa Mayor 2902            | 1       |
      | 2020-03-16 | 12:11:20    | Via Atlixcayotl 5718   | Cto Juan Pablo II 1920    | 4       |
      | 2020-03-16 | 15:12:20    | Benito Juarez 51-B     | Av 25 Poniente 1301       | 3       |
      | 2020-03-16 | 18:30:44    | C Real Sn Andres 4002  | Blvd Municipio Libre 1923 | 1       |
    And "John Smith" lodges a customer service request for lost object
    And the lost object is a "smart phone"
    And the date of the loss is "2020-03-16"
    When the operator enters the date of loss
    Then the operator should see 6 rows
    When the operator enters "Via Atlixcayotl 5718" as pickup address
    Then the operator should see 2 rows