import React from 'react';

import { TableHead, 
         TableRow, TableCell, TableBody, TextField, FormControlLabel, Checkbox, Grid, Button, ButtonGroup, Divider } from '@material-ui/core';

function ScamAnalysis() {
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <TextField id="incident-date" label="Incident date" type="date" InputLabelProps={{shrink: true}} />
          <TextField id="pick-up-address" label="Pick up address" fullWidth />
          <TextField id="drop-off-address" label="Drop off address" fullWidth />
          <TextField id="taxi-id" label="Taxi ID" />
        </Grid>
        <Grid item xs={6}>
          <FormControlLabel 
            control={<Checkbox id="overpriced25" />} 
            label="Overpriced trips >= 25%" />
          <FormControlLabel 
            control={<Checkbox id="overpriced50" />} 
            label="Overpriced trips >= 50%" />
          <p/>
          <ButtonGroup orientation="vertical" style={{width: "50%"}}>
            <Button id="submit-button" variant="outlined" color="primary">Submit</Button>
            <Button id="clear-form-button" variant="outlined" color="primary">Clear form</Button>
          </ButtonGroup>
        </Grid>
      </Grid>
      <p/>
      <Divider/>

      <table style={{width: "100%"}}>
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Pick up time</TableCell>
            <TableCell>Drop off time</TableCell>
            <TableCell>Est pick up time</TableCell>
            <TableCell>Est duration</TableCell>
            <TableCell>Pick up address</TableCell>
            <TableCell>Drop off address</TableCell>
            <TableCell>Taxi ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow className="data-row">
            <TableCell>2020-03-16</TableCell>
            <TableCell>10:28.30</TableCell>
            <TableCell>10:44.16</TableCell>
            <TableCell>10:30.00</TableCell>
            <TableCell>00:11.00</TableCell>
            <TableCell>Via Atlixcayotl 5718</TableCell>
            <TableCell>Blvd Atlixco 3304</TableCell>
            <TableCell>3</TableCell>
          </TableRow>
        </TableBody>
      </table>
    </div>
  );
}

export default ScamAnalysis;
