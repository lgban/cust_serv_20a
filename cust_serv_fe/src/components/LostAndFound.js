import React from 'react';

import { Button, Input, InputLabel, TableHead, 
         TableRow, TableCell, TableBody, Divider } from '@material-ui/core';

function LostAndFound() {
  return (
    <div>
      <form>
        <InputLabel htmlFor="date-of-trip">Incident date</InputLabel>
        <Input id="date-of-trip" type="date" />
        <p/>
        <InputLabel htmlFor="pick-up-address">Pick up address</InputLabel>
        <Input id="pick-up-address" fullWidth />
        <p/>
        <Button id="submit-button" variant="outlined" color="primary">Submit</Button>
      </form>
      <p/>
      <Divider/>
      <table style={{width: "100%"}}>
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Pick up time</TableCell>
            <TableCell>Pick up address</TableCell>
            <TableCell>Drop off address</TableCell>
            <TableCell>Taxi ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow className="data-row">
            <TableCell>2020-03-15</TableCell>
            <TableCell>16:20.10</TableCell>
            <TableCell>Via Atlixcayotl 5718</TableCell>
            <TableCell>Blvd Atlixco 3304</TableCell>
            <TableCell>3</TableCell>
          </TableRow>
        </TableBody>
      </table>
    </div>
  );
}

export default LostAndFound;
