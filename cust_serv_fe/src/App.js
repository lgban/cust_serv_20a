import React from 'react';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import LostAndFound from './components/LostAndFound';
import { Container } from '@material-ui/core';

function App() {
  return (
    <Router>
      <Container>
        <p/>
        <Switch>
          <Route path="/lost_and_found">
            <LostAndFound />
          </Route>
          <Route path="/scam_analysis">
            Scam Analysis
          </Route>
          <Route path="/">
            Hello
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
